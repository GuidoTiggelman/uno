package View;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class TUI{

    private BufferedReader bufferedReader;

    public void printMessage(String message){
        System.out.println(message);
    }

    public int readInt(){
        try{
            return Integer.parseInt(bufferedReader.readLine());
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    public String readString(){
        try{
            return bufferedReader.readLine();
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    public TUI(){
        this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }


}
