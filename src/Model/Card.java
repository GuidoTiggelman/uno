package Model;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Card{
    private Color color;
    private Symbol symbol;

    public Symbol getSymbol(){
        return symbol;
    }

    public Color getColor(){
        return color;
    }

    public void setColor(Color color){
        this.color=color;
    }

    public void setSymbol(Symbol symbol){
        this.symbol=symbol;
    }

    public Card (Color color,Symbol symbol){
        this.color = color;
        this.symbol = symbol;
    }

    /**
     * Enum with all the four game colors, alongside a W color for the wildcards.
     */
    public enum Color{
        B,
        Y,
        G,
        R,
        W
    }


    /**
     * Enum with all the symbols and their corresponding points
     */
    public enum Symbol{
        ZERO(0),
        ONE(1),
        TWO (2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        R(20),
        S(20),
        D(20),
        W(50),
        F(50);

        private int points;
        Symbol(int points){
            this.points = points;
        }

        public int getPoints(){
            return points;
        }
    }


    /**
     * Checks whether the card you want to play is an allowed card to play
     * @param wantToPlay the card that the player wants to play
     * @param player player that wants to play the card
     * @param topCard latest card that's added to the deck
     * @return true if wantToPlay is an allowed card to play
     */
    public boolean allowedMove(Card wantToPlay, Player player, Card topCard){
        if (wantToPlay.symbol.equals(Symbol.F)){
            for (Card card: player.getHand()){
                //You are not allowed to play a WildcardDraw4 card if you have the color of the current topCard in your hand.
                if (topCard.getColor().equals(card.getColor())){
                    return false;
                }
            }
            return true;
        }
        //You are always allowed to play a normal Wildcard
        else if (wantToPlay.symbol.equals(Symbol.W)){
            return true;
        }
        //If the card you want to play matches the color or symbol of the topCard, you are allowed to play it.
        else if (this.symbol.equals(wantToPlay.symbol) || this.color.equals(wantToPlay.color)){
            return true;
        }
        else {
            return false;
        }
    }

    public String toString(){
        return ""+color+" "+symbol+"";
    }

}
