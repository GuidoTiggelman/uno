package Model;

import Model.NetworkProtocol.Command;
import View.TUI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
public class Game implements Runnable{
    private ArrayList<Card> deck;
    private ArrayList<Card> discardPile;
    private ArrayList<Player> players;
    private int currentPlayerIndex;
    private boolean forwards;

    public ArrayList<Card> getDeck(){
        return deck;
    }

    public void setDeck(ArrayList<Card> deck){
        this.deck=deck;
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }

    public int getCurrentPlayerIndex(){
        return currentPlayerIndex;
    }
    public void setCurrentPlayerIndex(int currentPlayerIndex){
        this.currentPlayerIndex=currentPlayerIndex;
    }

    public boolean isForwards(){
        return forwards;
    }

    public void setForwards(boolean forwards){
        this.forwards=forwards;
    }

    public ArrayList<Card> getDiscardPile(){
        return discardPile;
    }

    public void setDiscardPile(ArrayList<Card> discardPile){
        this.discardPile=discardPile;
    }

    /**
     * Constructor makes a new discardPile and deck.
     * It also initializes the currentPlayerIndex and the forwards.
     * @param players to initialize the ArrayList of players
     */
    public Game(ArrayList<Player> players){
        this.players = players;
        this.discardPile = new ArrayList<>();
        this.forwards = true;
        this.currentPlayerIndex = 0;
        MakeDeck deck = new MakeDeck();
        this.deck = deck.cards;
    }


    /**
     * Gives a given amount of cards to a given player
     * @param player player to give the cards to
     * @param cardsCount amount of cards you want to give the players
     */
    public void giveCards(Player player,int cardsCount){
        //Check if there are enough cards left in the deck to give
        if (cardsCount > deck.size()){
            int cardsInDeck = deck.size();
            int cardsLeftToDraw = cardsCount-deck.size();
            Card topCard = showTopCard();
            //Give the player every card that was still in the deck
            for(int i=0;i<cardsInDeck;i++){
                player.getHand().add(deck.get(i));
            }

            //Refill the deck with all cards in the discardPile and adding the topCard to the discardPile.
            deck.addAll(discardPile);
            deck.remove(topCard);
            //This also clears the deck
            discardPile.clear();
            discardPile.add(topCard);
            //Give the player every card that he still needed to get
            for(int i=0;i<cardsLeftToDraw;i++){
                player.getHand().add(deck.get(i));
            }
            //Remove the cards that were given to the players from the deck.
            for(int i=0;i<cardsLeftToDraw;i++){
                deck.remove(i);
            }
        }
        else {
            //Create an Arraylist with the cards that the player should receive
            ArrayList<Card> currentGiven = new ArrayList<>();
            for(int i=0;i<cardsCount;i++){
                currentGiven.add(deck.get(i));
            }
            //Every card in the Arraylist should be added to the playerHand and be removed from the deck.
            for(Card card : currentGiven){
                player.getHand().add(card);
                int index = deck.indexOf(card);
                deck.remove(index);
            }
        }
    }

    /**
     * Checks if the round is over
     * @return true if one of the player has an empty hand.
     */
    public boolean roundIsOver(){
        for (Player player: players){
            if (player.getHand().size()==0){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the winner of the round
     * @return the player that has an empty hand.
     */
    public Player roundWinner(){
        for (Player player: players){
            if (player.getHand().size()==0){
                return player;
            }
        }
        return null;
    }

    /**
     * Checks if the game is over
     * @return true if one of the players has a score of 500 or more
     */
    public boolean gameIsOver(){
        for (Player player: players){
            if (player.getPoints() >= 500){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the player that won the game
     * @return the player that has 500 points or more
     */
    public String gameWinner(){
        for (Player player: players){
            if (player.getPoints() >= 500){
                return player.getPlayerName();
            }
        }
        return null;
    }

    /**
     * Returns the card that's on top of the discardPile
     * @return the topCard
     */
    public Card showTopCard(){
        return discardPile.get(discardPile.size()-1);
    }

    /**
     * Gives the round winner the amount of points from the cards of the other players
     */
    public void addPointsToRoundWinner(){
        int points = 0;
        if(roundIsOver()){
            for (Player player: players){
                for (Card card: player.getHand()){
                    points += card.getSymbol().getPoints();
                }
            }
        }
        roundWinner().setPoints(points);
    }

    /**
     * Prints the points of every player
     * @return each player with their according points.
     */
    public HashMap<String,Integer> printPoints(){
        HashMap<String, Integer> result = new HashMap<>();
        for (Player player: players){
            result.put(player.getPlayerName(), player.getPoints());
        }
        return result;
    }

    /**
     * Gives the index of the next player
     * @return the integer of the next player
     */
    public int getNextPlayerIndex(){
        if (this.forwards){
            //If the direction is forwards and the currentPlayer is the last player in the list of players,
            //the next player is the first one of the player list.
            if (currentPlayerIndex == (players.size()-1)){
                return 0;
            } else {
                return currentPlayerIndex+1;
            }
        }
        else {
            //If the direction is backwards and the currentPlayer is the first player in the list of players,
            //the next player is the last one of the player list.
            if (currentPlayerIndex == 0){
                return players.size()-1;
            } else {
                return currentPlayerIndex-1;
            }
        }
    }

    /**
     * Skips the next player
     */
    public void skipPlayer(){
        currentPlayerIndex = getNextPlayerIndex();
    }

    /**
     * Sets forwards to false if it was true and vice versa.
     */
    public void reverseDirection(){
        if (forwards){
            forwards = false;
        }
        else {
            forwards = true;
        }
    }

    /**
     * Returns the card that the player drew the last.
     * @param player the player that you want to get the lastCard from
     * @return the last card of the player
     */
    public Card getLastCard(Player player){
        return player.getHand().get(player.getHand().size()-1);
    }

    /**
     * Plays the last card of the playerHand
     * @param player the player that needs to play the last card
     */

    public void playLastCard(Player player){
        Card card = getLastCard(player);
        switch(card.getSymbol()){
            case S:
                skipPlayer();
                break;
            case R:
                reverseDirection();
                break;
            case D:
                giveCards(players.get(getNextPlayerIndex()),2);
                break;
            case W:
                player.setColorWildcard(card);
                break;
            case F:
                player.setColorWildcard(card);
                giveCards(players.get(getNextPlayerIndex()),4);
                break;
            default:
                break;
        }
        discardPile.add(card);
        player.getHand().remove(card);
        broadcast("Played card: "+ card);
    }

    /**
     * Plays the action of the card that the player wants to play.
     * @param player the player that plays the card
     */
    public void doMove(Player player){
        broadcast(player.getPlayerName() + ", please type the index of the card you want to play!");
        broadcast(player.getHand().toString());
        Card card = player.doMovePlayer(showTopCard());
        broadcast("Played card: "+ card);
        //Checks if the card can be played on the topCard
        if (showTopCard().allowedMove(card, player, showTopCard())){
            switch(card.getSymbol()){
                case S:
                    skipPlayer();
                    break;
                case R:
                    reverseDirection();
                    break;
                case D:
                    giveCards(players.get(getNextPlayerIndex()),2);
                    break;
                case W:
                    player.setColorWildcard(card);
                    break;
                case F:
                    player.setColorWildcard(card);
                    giveCards(players.get(getNextPlayerIndex()),4);
                    break;
                default:
                    break;
            }
            discardPile.add(card);
            player.getHand().remove(card);
            currentPlayerIndex = getNextPlayerIndex();
        }
        else{
            invalidMove(player);
        }
    }

    /**
     * Handles an invalid move
     * @param player the player that did an invalid move
     */
    public void invalidMove (Player player){
        broadcast("Invalid move, you get 1 card from the deck.");
        giveCards(player,1);
        broadcast(player.getPlayerName()+" drew the following card: "+ getLastCard(player));
        // Checks if the drawn card is an allowedCard to play directly.
        if(showTopCard().allowedMove(getLastCard(player),player, showTopCard())){
            //Asks the player if he wants to play the drawn card.
            String wantToPlay = player.playDrawnCard();
            if(wantToPlay.equals("yes")){
                playLastCard(player);
            }
        }
        currentPlayerIndex = getNextPlayerIndex();
    }

    /**
     * Plays the action of the topCard
     * @param player the player that needs to do the action of the topCard
     */
    public void doCard(Player player){
        Card card = showTopCard();
        switch(card.getSymbol()){
            case S:
                skipPlayer();
                currentPlayerIndex = getNextPlayerIndex();
                break;
            case R:
                reverseDirection();
                break;
            case D:
                giveCards(player,2);
                break;
            case W:
                System.out.println(player.getHand());
                player.setColorWildcard(card);
                break;
            default:
                break;
        }
    }

    /**
     * Lets players do their move while the game is not over
     */
    public void play(){
        broadcast(String.valueOf(Command.GAME_STARTED));
        while(!gameIsOver()){
            beforeEachRound();
            broadcast(String.valueOf(Command.ROUND_STARTED));
            while(!roundIsOver()){
                broadcast("----------------------------------------------------------------------------");
                doMove(players.get(currentPlayerIndex));
            }
            addPointsToRoundWinner();
            broadcast("Congratulations "+ roundWinner().getPlayerName()+ ", you have won the round\n" +
                    "The scores after this round are as follows: "
                    + printPoints());
            reset();

        }
        broadcast("");
        broadcast(gameWinner()+ " has won the game!");
    }

    /**
     * Makes sure that each player has 7 cards and the game has a topCard that is not a WildCardDraw4
     */
    public void beforeEachRound(){
        broadcast("");
        broadcast("NEW ROUND");
        broadcast("");
        for (Player player: players){
            giveCards(player,7);
        }
        Card card = deck.get(0);
        discardPile.add(card);
        deck.remove(0);
        while(showTopCard().getSymbol().equals(Card.Symbol.F)){
            this.deck.add(showTopCard());
            discardPile.remove(showTopCard());
            discardPile.add(this.deck.get(0));
            this.deck.remove(0);
        }
        doCard(players.get(currentPlayerIndex));
        Collections.shuffle(deck);
    }

    /**
     * Resets the deck and the playerHands so another round can be started
     */
    public void reset(){
        for (Player player : players){
            for(Card card: player.getHand()){
                this.deck.add(card);
                this.discardPile.remove(card);
            }
        }
        for (Player player: players){
            player.getHand().removeAll(player.getHand());
        }
        this.deck.addAll(this.discardPile) ;
        this.discardPile.clear();
        for (Card card : deck){
            //Every Wildcard should have the color UNIDENTIFIED before the round starts
            if (card.getSymbol().equals(Card.Symbol.F) || card.getSymbol().equals(Card.Symbol.W)){
                card.setColor(Card.Color.W);
            }
        }
    }

    public void broadcast(String input){
        for (Player player: players){
            player.sendMessage(input);
        }
    }

    @Override
    public void run(){
        play();
    }
}