package Model;

import Controller.ServerHandler;
import View.TUI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest{

    private Game game;
    private ArrayList<Player> players;
    private ArrayList<Card> cards;

    private ServerHandler serverHandler;

    /**
     * Three players with all 7 cards are added to the game before the tests start
     */
    @BeforeEach
    public void setUp(){
        cards = new ArrayList<>();
        players = new ArrayList<>();
        players.add(new ComputerPlayer("Player 1", cards, serverHandler));
        players.add(new ComputerPlayer("Player 2", cards, serverHandler));
        players.add(new ComputerPlayer("Player 3", cards, serverHandler));
        game = new Game(players);
        game.giveCards(players.get(0), 7);
        game.giveCards(players.get(1), 7);
        game.giveCards(players.get(2), 7);
    }

    @Test
    public void testGivePlayer7CardsWhenDeckNotFull(){
        game.giveCards(players.get(0), 7);
        assertEquals(14, game.getPlayers().get(0).getHand().size());
    }

    @Test
    public void testGivePlayer7CardsWhenDeckIsFull(){
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        cards.add(new Card(Card.Color.R, Card.Symbol.FOUR));
        cards.add(new Card(Card.Color.R, Card.Symbol.FOUR));
        cards.add(new Card(Card.Color.R, Card.Symbol.THREE));
        cards.add(new Card(Card.Color.R, Card.Symbol.THREE));
        cards.add(new Card(Card.Color.R, Card.Symbol.TWO));
        cards.add(new Card(Card.Color.R, Card.Symbol.ONE));
        game.setDiscardPile(cards);
        game.giveCards(game.getPlayers().get(0), 85);
        game.giveCards(game.getPlayers().get(1),7);
        assertEquals(14, game.getPlayers().get(1).getHand().size());

    }

    @Test
    public void testRoundIsOverWhenOnePlayerHasAnEmptyHand(){
        while (game.getPlayers().get(0).getHand().size() != 0){
            game.getPlayers().get(0).getHand().remove(0);
        }
        assertTrue(game.roundIsOver());
    }

    @Test
    public void testRoundIsOverWhenNoPlayerHasAnEmptyHand(){
        assertFalse(game.roundIsOver());
    }

    @Test
    public void testRoundWinnerWhenThereIsAWinner(){
        while(game.getPlayers().get(0).getHand().size()!=0){
            game.getPlayers().get(0).getHand().remove(0);
        }
        assertEquals(game.roundWinner(),game.getPlayers().get(0));
    }

    @Test
    public void testRoundWinnerWhenThereIsNoWinner(){
        assertNull(game.roundWinner());
    }

    @Test
    public void testGameWinnerWhenPlayerHasEnoughPoints(){
        game.getPlayers().get(0).setPoints(500);
        assertEquals( game.getPlayers().get(0).getPlayerName(),game.gameWinner());
    }

    @Test
    public void testGameWinnerWhenPlayerDoesNotHaveEnoughPoints(){
        game.getPlayers().get(0).setPoints(499);
        assertNull(game.gameWinner());
    }

    @Test
    public void testGameIsOver(){
        game.getPlayers().get(0).setPoints(500);
        assertTrue(game.gameIsOver());
    }

    @Test
    public void testGameIsNotOver(){
        game.getPlayers().get(0).setPoints(499);
        assertFalse(game.gameIsOver());
    }

    @Test
    public void testGetNextPlayerIndexWhenForwardsAndNotAtTheEnd(){
        game.setCurrentPlayerIndex(0);
        assertEquals(1, game.getNextPlayerIndex());
    }

    @Test
    public void testGetNextPlayerIndexWhenForwardsAndAtTheEnd(){
        game.setCurrentPlayerIndex(2);
        assertEquals(0, game.getNextPlayerIndex());
    }

    @Test
    public void testGetNextPlayerIndexWhenBackwardAndNotAtTheEnd(){
        game.setCurrentPlayerIndex(2);
        game.setForwards(false);
        assertEquals(1, game.getNextPlayerIndex());
    }

    @Test
    public void testGetNextPlayerIndexWhenBackwardsAndAtTheEnd(){
        game.setCurrentPlayerIndex(0);
        game.setForwards(false);
        assertEquals(2, game.getNextPlayerIndex());
    }

    @Test
    public void testReverseDirectionWhenForwardsIsTrue(){
        game.setForwards(true);
        game.reverseDirection();
        assertFalse(game.isForwards());
    }

    @Test
    public void testReverseDirectionWhenForwardsIsFalse(){
        game.setForwards(false);
        game.reverseDirection();
        assertTrue(game.isForwards());
    }

    @Test
    public void testPlayLastCardCaseS(){
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.S));
        game.playLastCard(game.getPlayers().get(0));
        assertEquals(1, game.getCurrentPlayerIndex());
    }

    @Test
    public void testPlayLastCardCaseR(){
        game.setForwards(true);
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.R));
        game.playLastCard(game.getPlayers().get(0));
        assertFalse(game.isForwards());
    }

    @Test
    public void testPlayLastCardCaseD(){
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.D));
        game.playLastCard(game.getPlayers().get(0));
        assertEquals(9, game.getPlayers().get(1).getHand().size());
    }

    @Test
    public void testPlayLastCardCaseW(){
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.W));
        game.playLastCard(game.getPlayers().get(0));
        assertFalse(game.showTopCard().getColor().equals(Card.Color.W));
    }

    @Test
    public void testPlayLastCardCaseF(){
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.F));
        game.playLastCard(game.getPlayers().get(0));
        assertFalse(game.showTopCard().getColor().equals(Card.Color.W));
        assertEquals(11, game.getPlayers().get(1).getHand().size());
    }

    @Test
    public void testPlayLastCardCaseDefault(){
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.EIGHT));
        game.playLastCard(game.getPlayers().get(0));
        assertEquals(Card.Color.R, game.showTopCard().getColor());
        assertEquals(7, game.getPlayers().get(0).getHand().size());
    }


    @Test
    public void testDoMoveS(){
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.S));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        game.doMove(players.get(0));
        assertEquals(2, game.getCurrentPlayerIndex());
    }
    @Test
    public void testDoMoveR(){
        game.setForwards(true);
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.R));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        game.doMove(players.get(0));
        assertEquals(0, game.getPlayers().get(0).getHand().size());
        assertFalse(game.isForwards());
    }
    @Test
    public void testDoMoveD(){
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.D));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        game.doMove(players.get(0));
        assertEquals(9, game.getPlayers().get(1).getHand().size());
    }
    @Test
    public void testDoMoveW(){
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.W));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        game.doMove(players.get(0));
        assertNotEquals(Card.Color.W, game.showTopCard().getColor());
    }
    @Test
    public void testDoMoveF(){
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.W, Card.Symbol.F));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        game.doMove(players.get(0));
        assertNotEquals(Card.Color.W, game.showTopCard().getColor());
        assertEquals(11, game.getPlayers().get(1).getHand().size());

    }
    @Test
    public void testDoMoveDefault(){
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.EIGHT));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        game.doMove(players.get(0));
        assertEquals(2, game.getDiscardPile().size());
        assertEquals(0, game.getPlayers().get(0).getHand().size());
        assertEquals(1, game.getCurrentPlayerIndex());
    }

    @Test
    public void testInvalidMoveWhenCardDrawnCannotBePlayed(){
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.EIGHT));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.Y, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        ArrayList<Card> deck = new ArrayList<>();
        deck.add(new Card(Card.Color.G, Card.Symbol.ONE));
        game.setDeck(deck);
        game.doMove(players.get(0));
        assertEquals(1, game.getDiscardPile().size());
        assertEquals(2, game.getPlayers().get(0).getHand().size());
        assertEquals(1, game.getCurrentPlayerIndex());
    }

    @Test
    public void testInvalidMoveWhenCardDrawnCanBePlayed(){
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(new Card(Card.Color.R, Card.Symbol.EIGHT));
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.Y, Card.Symbol.FIVE));
        game.setDiscardPile(cards);
        ArrayList<Card> deck = new ArrayList<>();
        deck.add(new Card(Card.Color.Y, Card.Symbol.ONE));
        game.setDeck(deck);
        game.doMove(players.get(0));
        assertEquals(2, game.getDiscardPile().size());
        assertEquals(1, game.getPlayers().get(0).getHand().size());
        assertEquals(1, game.getCurrentPlayerIndex());
    }

    @Test
    public void testDoCardW(){
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.Y, Card.Symbol.W));
        game.setDiscardPile(cards);
        game.doCard(players.get(0));
        assertNotEquals(Card.Color.W, game.showTopCard().getColor());
    }


    @Test
    public void testDoCardS(){
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.Y, Card.Symbol.S));
        game.setDiscardPile(cards);
        game.doCard(players.get(0));
        assertEquals(2, game.getCurrentPlayerIndex());
    }

    @Test
    public void testDoCardR(){
        game.setForwards(true);
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.Y, Card.Symbol.R));
        game.setDiscardPile(cards);
        game.doCard(players.get(0));
        assertFalse(game.isForwards());
    }

    @Test
    public void testPlay(){
        game.play();
        assertNotNull(game.gameWinner());
    }


    @Test
    public void testBeforeEachRound(){
        game.beforeEachRound();
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Card.Color.R, Card.Symbol.ZERO));
        game.setDiscardPile(cards);
        for (Player player: players){
            assertEquals(14 , player.getHand().size());
        }
        assertNotEquals(Card.Symbol.F, game.showTopCard().getSymbol());
        assertEquals(1, game.getDiscardPile().size());
    }


    @Test
    public void testReset(){
        game.reset();
        for (Player player: players){
            assertEquals(0, player.getHand().size());
        }
        assertEquals(108, game.getDeck().size());
    }
}
