package Model;

import Controller.ServerHandler;
import View.TUI;

import java.util.ArrayList;

public class HumanPlayer extends Player{
    private TUI tui;
    public HumanPlayer(String playerName,ArrayList<Card> hand,TUI tui,ServerHandler serverHandler){
        super(playerName,serverHandler);
        this.tui = tui;
    }

    /**
     * Lets the player choose a card of his hand to play
     * @param topCard the card that's the highest on the discardPile
     * @return the card that the player wants to play
     */
    @Override
    public Card doMovePlayer(Card topCard){
        int input = tui.readInt();
        sendMessage("");
        return this.getHand().get(input);
    }


    /**
     * Lets the player decide what color he wants to make the card
     * @param card the card that you want the set the color of
     */
    @Override
    public void setColorWildcard(Card card){
        sendMessage("What color do you want to make this card? Type one of the following numbers \n" +
                "1 = Red, 2 = Blue, 3 = Green, 4 = Yellow");
        int colorToAdd =0;
        colorToAdd = tui.readInt();
        switch(colorToAdd){
            case 1:
                card.setColor(Card.Color.R);
                break;
            case 2:
                card.setColor(Card.Color.B);
                break;
            case 3:
                card.setColor(Card.Color.G);
                break;
            default:
                card.setColor(Card.Color.Y);
                break;
        }
    }

    /**
     * Asks the player if he wants to play the drawn card and returns the answers.
     * @return answer
     */
    @Override
    public String playDrawnCard(){
        sendMessage("");
        sendMessage("Do you want to play this card? (yes/no)");
        sendMessage("");
        String wantToPlay = tui.readString();
        return wantToPlay;
    }

}
