Setup:
For this project I have used Oracle OpenJDK version 17.0.3. In order to make use of JSONObjects I imported the org.json package via the following way: File -> project structure -> libraries -> click + to add a library using Maven and search for org.json -> add it. Then in the sidebar go to modules -> click on your project -> select dependencies -> and make sure that it is listed there. I am also using Junit5.8.1 for this project.

Test class:
Inside of the Model package i have a class that is called GameTest. This class can be run in its entirety in order to check all of the test methods that are created inside of this class.

Playing a game:
The first step in running a game is running the server class inside of the Controller package.
After this you should edit the configuration of running the client, so that it allows multiple instances.
A client can type the following commands:

- hi (displayName) ([functionalities]) protocolVersion
.When a client wants to make a connection with the server the first argument should be hi. The client then has to type a space, because this is the delimiter. The second argument is the name of the player. The third argument are all theextra functionalities that your game support. Because my game doesn't support any functionalities, you can simply type null. The fourth argument is the protocolVersion that your game implement. The protocolVersion for my game is 1.2 !!

- join (gameSize)
.When a client wants to join a game, the first argument should be join. The second argument should be the amount of people that the client wants to play a game with. Note that this amount should be between 2 and 8.

- play (cardColor) (cardValue)
.When a client wants to play a card, the first argument should be play. The second argument should be the color of the card that you want to play and the third argument should be the value of the card you want to play.

- draw
.When a client wants to draw a card, the first argument should be draw.
